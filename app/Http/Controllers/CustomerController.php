<?php

namespace App\Http\Controllers;

use App\Models\Customer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CustomerController extends Controller
{

    public function getAll()
    {
        return response()->json(Customer::all());
    }

    public function getAllWithOnlyName()
    {
        return response()->json(
            DB::table('customers')
                ->join('users', 'customers.user_id', '=', 'users.id')
                ->select('customers.id', 'users.name')
                ->get()
        );    
    }

    public function get($id)
    {
        return response()->json(Customer::find($id));
    }

    public function getWithOnlyName($id)
    {
        return response()->json(
            DB::table('customers')
                ->where('customers.id', '=', $id)
                ->join('users', 'customers.user_id', '=', 'users.id')
                ->select('customers.id', 'users.name')
                ->first()
        );    
    }

    public function create(Request $request)
    {
        $customer = Customer::create($request->all());

        return response()->json($customer, 201);
    }

    public function update($id, Request $request)
    {
        $customer = Customer::findOrFail($id);
        $customer->update($request->all());

        return response()->json($customer, 200);
    }

    public function delete($id)
    {
        Customer::findOrFail($id)->delete();
        return response('Deleted Successfully', 200);
    }
}
