<?php

namespace App\Http\Controllers;

use App\Models\DriverGroup;
use Illuminate\Http\Request;

class DriverGroupController extends Controller
{

    public function getAll()
    {
        return response()->json(DriverGroup::all());
    }

    public function get($id)
    {
        return response()->json(DriverGroup::find($id));
    }

    public function create(Request $request)
    {
        $driverGroup = DriverGroup::create($request->all());

        return response()->json($driverGroup, 201);
    }

    public function update($id, Request $request)
    {
        $driverGroup = DriverGroup::findOrFail($id);
        $driverGroup->update($request->all());

        return response()->json($driverGroup, 200);
    }

    public function delete($id)
    {
        DriverGroup::findOrFail($id)->delete();
        return response('Deleted Successfully', 200);
    }
}
