<?php

namespace App\Http\Controllers;

use App\Models\Driver;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class DriverController extends Controller
{

    public function getAll()
    {
        return response()->json(Driver::all());
    }

    public function getAllWithOnlyName()
    {
        return response()->json(
            DB::table('drivers')
                ->join('users', 'drivers.user_id', '=', 'users.id')
                ->select('drivers.id', 'users.name')
                ->get()
        );    
    }

    public function get($id)
    {
        return response()->json(Driver::find($id));
    }

    public function getWithOnlyName($id)
    {
        return response()->json(
            DB::table('drivers')
                ->where('drivers.id', '=', $id)
                ->join('users', 'drivers.user_id', '=', 'users.id')
                ->select('drivers.id', 'users.name')
                ->first()
        );    
    }

    public function create(Request $request)
    {
        $driver = Driver::create($request->all());

        return response()->json($driver, 201);
    }

    public function update($id, Request $request)
    {
        $driver = Driver::findOrFail($id);
        $driver->update($request->all());

        return response()->json($driver, 200);
    }

    public function delete($id)
    {
        Driver::findOrFail($id)->delete();
        return response('Deleted Successfully', 200);
    }
}
