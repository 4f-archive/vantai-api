<?php

namespace App\Http\Controllers;

use App\Models\VehicleType;
use Illuminate\Http\Request;

class VehicleTypeController extends Controller
{

    public function getAll()
    {
        return response()->json(VehicleType::all());
    }

    public function get($id)
    {
        return response()->json(VehicleType::find($id));
    }

    public function create(Request $request)
    {
        $vehicleType = VehicleType::create($request->all());

        return response()->json($vehicleType, 201);
    }

    public function update($id, Request $request)
    {
        $vehicleType = VehicleType::findOrFail($id);
        $vehicleType->update($request->all());

        return response()->json($vehicleType, 200);
    }

    public function delete($id)
    {
        VehicleType::findOrFail($id)->delete();
        return response('Deleted Successfully', 200);
    }
}
