<?php

namespace App\Http\Controllers;

use App\Models\Order;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class OrderController extends Controller
{

    public function getAll()
    {
        return response()->json(Order::all());
    }

    public function get($id)
    {
        return response()->json(Order::find($id));
    }

    public function getWaitingOrdersCount($driverId)
    {
        $id = DB::table('drivers')->where('user_id', $driverId)->value('id');

        return response()->json(
            DB::table('orders')
                ->where('driver_id', '=', $id)
                ->where('status', '=', 1)
                ->count()
        );
    }

    public function getOrdersData()
    {
        $w = DB::table('orders')->where('status', 1)->count();
        $o = DB::table('orders')->where('status', 2)->count();
        $s = DB::table('orders')->where('status', 3)->count();
        $f = DB::table('orders')->where('status', 4)->count();

        return response()->json( [
            'waiting' => $w, 
            'on_delivery' => $o, 
            'success' => $s, 
            'failed' => $f
        ], 201);
    }

    public function create(Request $request)
    {
        $order = Order::create($request->all());

        return response()->json($order, 201);
    }

    public function update($id, Request $request)
    {
        $order = Order::findOrFail($id);
        $order->update($request->all());

        return response()->json($order, 200);
    }

    public function delete($id)
    {
        Order::findOrFail($id)->delete();
        return response('Deleted Successfully', 200);
    }
}