<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router->group(['prefix' => 'api', 'middleware' => 'auth'], function () use ($router) {
    // Profile
    $router->get('auth/me', 'AuthController@me');

    // Categories
    $router->get('users', 'UserController@getAll');
    $router->get('users/{id}', 'UserController@get');
    $router->post('users', 'UserController@create');
    $router->put('users/{id}', 'UserController@update');
    $router->delete('users/{id}', 'UserController@delete');

    // Categories
    $router->get('categories', 'CategoryController@getAll');
    $router->get('categories/{id}', 'CategoryController@get');
    $router->post('categories', 'CategoryController@create');
    $router->put('categories/{id}', 'CategoryController@update');
    $router->delete('categories/{id}', 'CategoryController@delete');

    // Orders
    $router->get('orders', 'OrderController@getAll');
    $router->get('orders/data', 'OrderController@getOrdersData');
    $router->get('orders/{id}', 'OrderController@get');
    $router->get('orders/{driverId}/waitingorderscount', 'OrderController@getWaitingOrdersCount');
    $router->post('orders', 'OrderController@create');
    $router->put('orders/{id}', 'OrderController@update');
    $router->delete('orders/{id}', 'OrderController@delete');

    // Customer
    $router->get('customers', 'CustomerController@getAll');
    $router->get('customers/onlyname', 'CustomerController@getAllWithOnlyName');
    $router->get('customers/{id}', 'CustomerController@get');
    $router->get('customers/{id}/onlyname', 'CustomerController@getWithOnlyName');
    $router->post('customers', 'CustomerController@create');
    $router->put('customers/{id}', 'CustomerController@update');
    $router->delete('customers/{id}', 'CustomerController@delete');

    // Groups
    $router->get('driver-groups', 'DriverGroupController@getAll');
    $router->get('driver-groups/{id}', 'DriverGroupController@get');
    $router->post('driver-groups', 'DriverGroupController@create');
    $router->put('driver-groups/{id}', 'DriverGroupController@update');
    $router->delete('driver-groups/{id}', 'DriverGroupController@delete');

    // Vehicle types
    $router->get('vehicle-types', 'VehicleTypeController@getAll');
    $router->get('vehicle-types/{id}', 'VehicleTypeController@get');
    $router->post('vehicle-types', 'VehicleTypeController@create');
    $router->put('vehicle-types/{id}', 'VehicleTypeController@update');
    $router->delete('vehicle-types/{id}', 'VehicleTypeController@delete');

    // Drivers
    $router->get('drivers', 'DriverController@getAll');
    $router->get('drivers/onlyname', 'DriverController@getAllWithOnlyName');
    $router->get('drivers/{id}', 'DriverController@get');
    $router->get('drivers/{id}/onlyname', 'DriverController@getWithOnlyName');
    $router->post('drivers', 'DriverController@create');
    $router->put('drivers/{id}', 'DriverController@update');
    $router->delete('drivers/{id}', 'DriverController@delete');
});

$router->group(['prefix' => 'api'], function () use ($router) {
    $router->post('auth/register', 'AuthController@register');
    $router->post('auth/login', 'AuthController@login');
});