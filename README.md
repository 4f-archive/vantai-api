# vantai-api

## Serve server
```
php -S localhost:8000 -t public
```

# Generate secret key
```
php artisan jwt:secret
```

This will update your .env file with something like JWT_SECRET=foobar